/* porovnanie trvania odoslania retazca do serioveho portu pomocou printf() a putc() */

#include "mbed.h"

Serial device (PA_2, PA_3, 115200); /* vytvorenie objektu triedy Serial pre konfiguraciu UARTU na
                                       pinoch PA2(TX) a PA3(RX) s baudrate 115200 Bd/s*/
Timer t; // inicializacia casovaca (triedy mbed, nie periferie procesora)

int main() {

    // retazec ako pole znakova
    char greeting[13] = {'H', 'e', 'l', 'l', 'o', ' ', 'W','o','r','l','d','\n','\r'};

    t.start(); // spustenie casovaca
    device.printf("Hello World\n\r"); // odoslanie retazca pomocou funkcie printf()
    t.stop(); // zastavenie casovaca
    device.printf("time of printf function is %dus\n\r", t.read_us()); // odoslanie casoveho udaju

    t.reset(); // vynulovanie casovaca

    t.start(); // spustenie casovaca
    for (int i = 0; i < 13; i++){ //odoslanie retazca pomocou funkcie putc()
        device.putc(greeting[i]);
    }
    t.stop(); // zastavenie casovaca
    device.printf("time of putc function is %dus\n\r", t.read_us()); // odoslanie casoveho udaju

    while(1){} // nekonecny cyklus

}
