/* na pinoch PA4, PB6 a PA7 blikaju LED pomocou troch nezavislych generatorov PWM*/

#include "mbed.h" // zahrnutie kniznice mbed do programu

/* vytvorenie 3 objektov triedy PwmOut pre konfiguraciu pinov PA_4, PB6 a PA7 ako generatorov PWM
   kazdy pin je naviazany na iny timer, PWM mozu mat roznu periodu aj striedu (pinout diagram F303) */
PwmOut led1(PA_4);
PwmOut led2(PB_6);
PwmOut led3(PA_7);

int main() {
    // nastavenie periody blikania na hodnoty v ms
    led1.period_ms(100);
    led2.period_ms(500);
    led3.period_ms(1000);
    
    // nastavenie striedy na 50%
    led1 = 0.5;
    led2 = 0.5;
    led3 = 0.5;

    /* generovanie PWM prebieha hardverovo, aby program bezal,
       staci prazdny nekonecny cyklus */
    while(1){
        wait_ms(10);
    }

}
