/* Pouzitie viacerych casovacov v jednom programe */

#include "mbed.h"

Serial device (PA_2,PA_3); // TX pin, RX pin

// inicializacia 3 casovacov (triedy mbed, nie periferie procesora)
Timer t1;
Timer t2;
Timer t3;

int main() {
    device.baud(115200); // nastavenie baudrate na 115200 Bd/s
    t1.start(); // spustenie prveho casovaca
    wait_us(10); // cakanie 10 us
    t1.start(); // opakovane spustenie 1 casovaca nema ziaden efekt
    t2.start(); // spustenie 2 casovaca
    wait_us(10); // cakanie 10 us
    t3.start(); // spustenie 3 casovaca
    wait_us(10); // cakanie 10 us
    t1.stop(); // zastavenie 1 casovaca
    t2.stop(); // zastavenie 2 casovaca
    t3.stop(); // zastavenie 3 casovaca
    // odoslanie informacii o trvani nameranych casov
    device.printf("\n\rtime is %d %d %d \n\r", t1.read_us(), t2.read_us(), t3.read_us());

    while(1){} // nekonecny cyklus

}
