/* ukazka ako bouncing sposobi ze sa zdetekuje viac stlaceni, ako realne ich realne bolo
   na pine PA4 sa detekuju spadove hrany(stlacenia) a po kazdych desiatich LED zablika 10/krat */

#include "mbed.h"

InterruptIn button(PA_4, PullUp);  /* digitalny vstup s moznostou hardveroveho
                                     prerusenia na PA4 v mode pull-up */
DigitalOut led(PA_5); // digitalny vystup na PA_5 s pociatocnou log.0 na vystupe

volatile int cnt = 0; /* pocet detekovanych spadovych hran, pouziva sa v obsluhe
                         prerusenia, musi byt oznacena volatile */

/* funkcia obsluhy prerusenia nema vstupne ani navratove parametre,
  pri detekcii spadovej hrany(stlacenia) sa inkrementuje */
void detect_falling_edge(){
    cnt++;
}

// bliknutie n-krat na pine PA5
void flash_n_times(int n){
    for (int i  = 0; i < n; i++){
        led = 1;
        wait_ms(100);
        led = 0;
        wait_ms(100);
    }
}

int main() {
    flash_n_times(1); // indikacia, ze program zacal
    // priradenie funkcie prerusenia pri spadovej hrane na PA4
    button.fall(&detect_falling_edge);
    while(1){
        if(cnt >= 10){ // ak je pocet napocitanych spadovych hran 10
            button.disable_irq(); // zakazanie prerusenia na PA4
            cnt = 0;  // vynulovanie poctu napocitanych spadovych hran
            flash_n_times(10);  // blinkutie LED 10-krat na PA5
            button.enable_irq();// povolenie prerusenia na PA4
        }
    }
    
}
