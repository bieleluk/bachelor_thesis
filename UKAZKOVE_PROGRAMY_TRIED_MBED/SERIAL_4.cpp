/* program, ktory demonstruje funkcionalitu scanf() */

#include "mbed.h"

// makro, ktore vracia vacsi z 2 vstupnych parametrov
#define max(a, b) (((a) > (b)) ? (a) : (b))

/* vytvorenie 2 objektov triedy PwmOut pre konfiguraciu pinov PA_4 a PA7 ako generatorov PWM
   kazdy pin je naviazany na iny timer, PWM mozu mat roznu periodu aj striedu (pinout diagram) */
PwmOut led1(PA_4);
PwmOut led2(PA_7);

Serial pc (PA_2, PA_3, 115200); /* vytvorenie objektu triedy Serial pre konfiguraciu UARTU na
                                   pinoch PA2(TX) a PA3(RX) s baudrate 115200 Bd/s */

int main(){

    int correct_inputs; // pocet priradenych parametrov precitanych zo scanf()
    int period1_ms = 1; // perioda PWM musi byt kladna
    int period2_ms = 1;

    led1 = 0.5; // strieda 50%
    led2 = 0.5;

    pc.printf("\ec"); // vymazanie celeho terminaloveho okna
    wait_ms(50); // cakanie, kym vymazanie prebehne
    // odoslanie formatovaneho retazca do serioveho portu
    pc.printf("insert two positive integers divided by space, periods in ms\n\r");

    while(1){

        while(!pc.readable()){} // cakanie, kym nie je nejaky znak na precitanie

        // citanie znakov, kym nie je poruseny ocakavany format a nasledne priradenie hodnot premennym
        correct_inputs = pc.scanf("%d %d", &period1_ms, &period2_ms);

        // podla poctu priradenych parametrov sa odosle sprava do COM portu
        if (correct_inputs == 0){
            pc.printf("you have wrongly inserted the first argument");
        }else if (correct_inputs == 1){
            pc.printf("you have wrongly inserted the second argument");
        }else{
            pc.printf("you have correctly inserted both arguments");
        }

        // nastavenie periody PWM tak, aby bola kladna
        led1.period_ms(max(1,period1_ms));
        led2.period_ms(max(1,period2_ms));

        // odoslanie hodnot period PWM do COM portu
        pc.printf("\n\rreal period of led1 is %dms\n\rreal period of led2 is %dms\n\rinsert two positive integers divided by space, periods in ms\n\r", max(1,period1_ms), max(1,period2_ms));

    }
}
