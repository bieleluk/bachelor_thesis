/* zmena stavu 2 LED na PA6 a PA7 podla prijatych znakov zo serioveho poru s
   vyuzitim prerusenia po prijati znaku */

#include "mbed.h"

// digitalny vystup na PA_6 a PA7 s pociatocnou log.1 na vystupe
DigitalOut led1(PA_6, 1);
DigitalOut led2(PA_7, 1);

Serial device(PA_2, PA_3, 115200); /* vytvorenie objektu triedy Serial pre konfiguraciu UARTU na
                                     pinoch PA2(TX) a PA3(RX) s baudrate 115200 Bd/s */

/* funkcia obsluhy prerusenia nema vstupne ani navratove parametre,
   po prijati znaku sa funkcia zavola, funkcia precita prijaty znak
   a podla neho meni stav LED */
void read_sign(){
    char sign = device.getc(); // precita sa jeden znak z COM portu
    switch (sign) {
        case '1': // ak bol znak '1'
            led1 = !led1; // zmeni sa stav LED1
            break;
        case '2': // ak bol prijaty znak znak '2'
            led2 = !led2; // zmeni sa stav LED2
            break;
        default:
            break;
    }
 }

int main() {
    // odoslanie formatovaneho retazca do serioveho portu
    device.printf("insert 1 to change state of LED1 or 2 to change state of LED2\n\r");

    device.attach(&read_sign); //priradenie funkcie obsluhy prerusenia vyvolaneho po prijati znaku

    while(1) {
        // poslanie aktualneho stavu LED do serioveho portu
        device.printf("LED1 = %d\tLED2 = %d\r", led1.read(), led2.read());
        wait(0.1); //cakanie 100ms
    }

}
