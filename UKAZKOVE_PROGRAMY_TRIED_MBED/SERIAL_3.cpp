/* do serioveho portu sa budu vypisovat zadane nezaporne cele
   cisla oddelene medzerou */

#include "mbed.h"

DigitalOut led(PA_5); // digitalny vystup na PA_5 s pociatocnou log.0 na vystupe

Serial device(PA_2, PA_3, 115200); /* vytvorenie objektu triedy Serial pre konfiguraciu UARTU na
                                      pinoch PA2(TX) a PA3(RX) s baudrate 115200 Bd/s */

volatile char last_printed_char = ' '; /* hodnota posledneho odoslaneho znaku, pretoze sa pouziva
                                      v obsluhe prerusenia, musi byt pouzite volatile */

/* funkcia obsluhy prerusenia nema vstupne ani navratove parametre,
   po prijati znaku sa funkcia zavola, rozsvieti sa LED, precita sa prijaty znak
   a podla neho bud odosle dalsiu cislicu cisla, odosle medzeru medzi cislami,
   na konci LED zhasne */
void send_back(){
    led = 1; // rozsvieti sa LED
    char sign = device.getc(); // precita sa prijaty znak

    // ak prijaty znak nie je cislica a medzera uz bola poslana, funkcia sa ukonci
    if((sign < 48 || sign > 57) && last_printed_char == ' '){
        led = 0; // LED zhasne
        return;
    }else if ((sign < 48 || sign > 57)){ // ak prijaty znak nie je cislica, odosle sa medzera
        sign = ' ';
    }

    device.putc(sign); // odoslanie znaku
    last_printed_char = sign; // priradenie posledneho odoslaneho znaku premennej
    led = 0; // LED zhasne
}

int main() {
    // odoslanie formatovaneho retazca do serioveho portu
    device.printf("\n\rInsert nonnegative numbers separated with ' '\n\r");

    device.attach(&send_back); //priradenie funkcie obsluhy prerusenia vyvolaneho po prijati znaku

    while(1) { // nekonecny cyklus
        wait_ms(1);
    }

}
