/* na pine PA7 blika LED s periodou 800ms */

#include "mbed.h"           // zahrnutie kniznice mbed do programu

DigitalOut myled(PA_7, 1);  /* vytvorenie objektu triedy DigitalOut pre konfiguraciu pinu PA_7
                               ako digitalne-vystupneho s pociatocnou hodnotou log.1 na vystupe */
int main() {
    float period = 0.8;     // perioda blikania v sekundach
    while(1) {              // nekonecny cyklus
        wait(period/2);     // cakanie 0.4 sekundy - poLperioda
        myled = !myled;     // inverzia log. hodnoty vystupu PA7
    }
}
