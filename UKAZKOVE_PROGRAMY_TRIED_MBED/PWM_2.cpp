/* zmena jasu LED na PA4 pomocou trimmru na PA1 */

#include "mbed.h" // zahrnutie kniznice mbed do programu

PwmOut led(PA_4); /* vytvorenie objektu triedy PwmOut pre konfiguraciu
                     pinu PA_4 ako generatora PWM */

AnalogIn trimmer(PA_1); /* vytvorenie objektu triedy AnalogIn pre
                           konfiguraciu pinu PA_1 ako analogoveho vstupu */

int main() {
    led.period(0.01); // nastavenie periody PWM na PA4 na 10ms

    while(1) {
        led = trimmer; /* nastavenie striedy na hodnotu umernu
                          vstupnemu apatiu na PA1 */
    }

}
