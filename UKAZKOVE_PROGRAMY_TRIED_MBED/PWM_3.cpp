/* prehratie zakladnych tonov stupnice pomocou piezo buzzera na PA6 */

#include "mbed.h" // zahrnutie kniznice mbed do programu

PwmOut buzzer(PA_6); /* vytvorenie objektu triedy PwmOut pre konfiguraciu
                        pinu PA6 ako generatora PWM */

float frequencies[8] = {261, 293, 329, 349, 392, 440, 494, 523}; //pole frekvencii tonov od c1 do c2
int num_of_freq = 8; // pocet prvkov pola frequencies
int main() {
    buzzer = 0.5; //nastavenie stridy na 50%,
    uint8_t i = 0; //premenna pomocou ktorej budeme prechadzat pole
    while(1) {
        buzzer.period(1./frequencies[i]); // nastavenie periody PWM (T = 1/f)
        i++; // prejdenie na dalsi ton
        i %= num_of_freq; // po prejdeni vsetkych tonov sa zacne opat od zaciatku
        wait(1);  // cakanie 1 sekundu
    }
}
