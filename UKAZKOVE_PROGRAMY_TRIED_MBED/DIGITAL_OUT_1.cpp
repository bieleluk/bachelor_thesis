/* na pine PA5 blika LED s frekvenciou 1Hz */

#include "mbed.h"      // zahrnutie kniznice mbed do programu

DigitalOut led (PA_5); // vytvorenie objektu triedy DigitalOut pre konfiguraciu pinu PA_5 ako digitalne-vystupneho

int main() {

    while(1) {          //nekonecny cyklus
        led = 1;        // log.1 na vystup pinu PA5->rozsvietenie LED
        wait_ms(500);   // cakanie 0.5 sekundy - prva pooperioda
        led = 0;        // log.0 na vystup pinu PA5->zhasnutie LED
        wait_ms(500);   // cakanie 0.5 sekundy - druha polperioda
    }

}
