/* zaokruhlovanie striedy pri malej periode PWM */

#include "mbed.h" // zahrnutie kniznice mbed do programu
PwmOut led(PA_6); /* vytvorenie objektu triedy PwmOut pre konfiguraciu
                     pinu PA6 ako generatora PWM */

int main() {
    led.period(0.000002); /* nastavenie periody PWM na 2us, strieda moze nadobudat hodnoty
                             iba 0, 50% alebo 100% */
    while(1) {
        led = 0.24; // strieda sa zaokruhli na 0%, LED nesvieti
        wait(1);
        led = 0.25; // strieda sa zaokruhli na 50%, LED svieti
        wait(1);
        led = 0.75; // strieda sa zaokruhli na 100%, LED svieti a ma vyssi jas
        wait(1);
    }
}
