/* LED pine PA5 svieti ak je tlacidlo na pine PA4 stlacene*/

#include "mbed.h"       // zahrnutie kniznice mbed do programu

DigitalIn button(PA_4); // vytvorenie objektu triedy DigitalIn pre konfiguraciu pinu PA_4 ako digitalne-vstupneho
DigitalOut led(PA_5);   // vytvorenie objektu triedy DigitalOut pre konfiguraciu pinu PA_5 ako digitalne-vystupneho

int main() {
    button.mode(PullUp);    // nastavenie pinu PA_4 do pull-up modu, ak tlacidlo nie je stlacene, na vstupe je log.1
    while(1){               // nekonecny cyklus
        if(button == 0){    // podmienka if-else
            led = 1;        // ak je tlacidlo stlacene rozsvieti sa LED
        }else{
            led = 0;        // v opacnom pripade LED nesvieti
        }
    }
    /* //while cyklus je mozne nahradit zakomentovanym while cyklom, kde je pouzity skrateny zapis funkcii
    while(1){               // nekonecny cyklus
        led = !button;      // ak je tlacidlo stlacene, led bude svietit
    }*/
}
