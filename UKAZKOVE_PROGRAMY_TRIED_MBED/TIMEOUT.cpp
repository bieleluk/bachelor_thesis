/* tester reakcie, medzi pin PA4 a zem je pripojene tlacidlo, na PA5 je pripojena LED */

#include "mbed.h"

#define MAX_WAIT_TIME_MS 3000 //
#define MIN_WAIT_TIME_MS 1000
#define DEBOUNCE_TIME 0.005
#define MAX_REFLEX_TIME_MS 1000


InterruptIn button(PA_4, PullUp); /* digitalny vstup s moznostou hardveroveho
                                     prerusenia na PA4 v mode pull-up */

DigitalOut led(PA_5); // digitalny vystup na PA_5 s pociatocnou log.0 na vystupe

AnalogIn rand_seed(PA_6);  /* analogovy vstup na nezapojenom pine pre
                              generovanie pseudonahodneho cisla */

Timeout countdown; // vytvorenie objektu triedy Timeout

// pocet stlaceni v casovom limite
volatile int cnt = 0;
// 0-caka sa na stlacenie, 1-sltacenie prebehlo v limite, 2-koniec hry
volatile int game_state;


void flash(int n){ // LED na PA5 blikne n-krat
    wait(1); // cakanie 1s pre jednoznacne identifikovanie poctu bliknuti
    for(int i = 0; i < 2*n; i++){
        led = !led;
        wait(0.2);
    }
}

/* funkcia obsluhy prerusenia nema vstupne ani navratove parametre,
   po uplynuti reakcneho casu sa zakaze prerusenie vyvolane tlacidlom,
   LED zhasne a hra sa dostane do stavu 2->koniec hry */
void end_of_game(){
    button.disable_irq(); // zakazanie prerusenia vyvolaneho tlacidlom
    led = 0;              // zhasnutie LED
    game_state = 2;       // stav hry -> koniec hry
}

/* funkcia obsluhy prerusenia nema vstupne ani navratove parametre,
   po stlaceni tlacidla LED zhasne, zakaze sa prerusenie vyvolane tlacidlom,
   pocka sa kym sa tlacidlo pusti (s debouncingom), inkrementuje sa pocet
   dobrych reakcii a hra sa dostane do stavu 1->uspesna reakcia */
void push(){
    led = 0;              // zhasnutie LED
    countdown.detach();   // zakazanie prerusenia vyvolaneho objektom countdown
    button.disable_irq(); // zakazanie prerusenia vyvolaneho tlacidlom
    wait(DEBOUNCE_TIME);  // debouncing
    while(button == 0){}  // cakanie na pustenie tlacidla
    wait(DEBOUNCE_TIME);  // debouncing
    cnt ++;               // pocet dobrych reakcii sa inkrementuje
    game_state = 1;       // stav hry -> uspesna reakcia
}

int main() {
    button.fall(&push);     // priradenie funkcie prerusenia pri stlaceni tlacidla
    button.disable_irq();   // zakazanie prerusenia vyvolaneho tlacidlom
    // inicializacia pseudonahodneho generatora pseudonahodnym cislom(citanim napatia na nezapojenom vstupe)
    srand (rand_seed.read_u16());

    while(1){
        game_state = 0;
        // cakanie nahodnej doby v rozmedzi <MIN_WAIT_TIME_MS , MAX_WAIT_TIME_MS>, po ktorom sa LED rozsvieti
        wait_ms(MIN_WAIT_TIME_MS + rand()%(MAX_WAIT_TIME_MS - MIN_WAIT_TIME_MS));
        led = 1;              // rozsvitenie LED
        button.enable_irq();  // povolenie prerusenia vyvolaneho tlacidlom
        // priradenie funkcie prerusenia, ktore sa vyvola po uplynuti MAX_REFLEX_TIME_MS
        countdown.attach_us(&end_of_game, 1000 * MAX_REFLEX_TIME_MS);
        while(game_state == 0){} // cakanie kym sa stlaci tlacidlo alebo uplynie reakcny cas
        if (game_state == 1){ // ak bolo tlacidlo stlacene pokracuje sa v hre
            continue;
        }else{                // ak uplynul reakcny cas, hra sa konci
            break;
        }
    }
    flash(cnt); // bliknutie LED poctom uspesnych reakcii
    while(1){}

}
