/* zmena stavu 2 LED na PA6 a PA7 podla prijatych znakov zo serioveho poru */

#include "mbed.h"

// digitalny vystup na PA_6 a PA7 s pociatocnou log.1 na vystupe
DigitalOut led1(PA_6, 1);
DigitalOut led2(PA_7, 1);

Serial device(PA_2, PA_3);  /* vytvorenie objektu triedy Serial pre konfiguraciu UARTU na
                               pinoch PA2(TX) a PA3(RX) */

int main() {

    device.baud(115200); // nastavenie baudrate na 115200 Bd/s

    // poslanie formatovaneho retazca do serioveho portu
    device.printf("push 1 to turn on LED1 or push 2 to turn on LED2\n\r");

    char sign; // premenna typu char, do ktorej sa bude ukladat precitany znak
    while(1) {
        if(device.readable()) { //kontrola ci je nejaky znak na precitanie
            sign = device.getc(); // precita sa jeden znak

            switch (sign) {
                case '1': // ak bol prijaty znak '1'
                    led1 = !led1; // zmeni sa stav LED1
                    device.printf("LED1 = %d\n\r", led1.read()); // odosle sa aktualny stav led1
                    break;
                case '2': // ak bol prijaty znak znak '2'
                    led2 = !led2; // zmeni sa stav LED2
                    device.printf("LED2 = %d\n\r", led2.read()); // odosle sa aktualny stav led2
                    break;
                default:
                    break;
            }

        }
    }

}
