/* odmeriavanie rovnakeho casoveho useku s a bez pouzitia funkcie stop */

#include "mbed.h"

Serial device (PA_2,PA_3, 115200); /* vytvorenie objektu triedy Serial pre konfiguraciu UARTU na
                                      pinoch PA2(TX) a PA3(RX) s baudrate 115200 Bd/s */
Timer t; // inicializacia casovaca (triedy mbed, nie periferie procesora)

int main() {

    t.start(); // spustenie casovaca
    wait(0.00001); // cakanie 10us
    t.stop(); // zastavenie casovaca
    device.printf("\n\rTime with stop function used is %dus\n\r", t.read_us()); // odoslanie casoveho udaju

    t.reset(); // vynulovanie casovaca

    t.start(); // spustenie casovaca
    wait_us(0.00001); // cakanie 10us
    int time = t.read_us(); // precitanie trvania bez pouzitia funkcie stop()
    device.printf("\n\rTime without stop function used is %dus\n\r", time); // odoslanie casoveho udaju
    
    while(1){}

}
