#include "Debug.h" // DEBUG_F042F6P6 library must imported to program

PwmOut led(PA_4); // PWM generator on PA4
Debug_complete pc(PA_2,PA_3, 115200);  // TX pin, RX pin and baudrate of UART

void turn_PLL_HSE_on(); // Function to turn PLL_HSE on, external HSE must be connected
void turn_PLL_HSI_on(); // Function to turn PLL_HSI on
void turn_HSI48_on(); // Function to turn HSE48 on

int main() {
    
    led.period(1); // Set period of PWM on PA4 to 1 s
    led = 0.5; // Set duty cycle of PWM on PA4 to 50%
    pc.breakpoint(__LINE__); // breakpoint with actual line number parameter
    turn_PLL_HSE_on(); // external HSE must be connected
    pc.breakpoint(__LINE__); // breakpoint with actual line number parameter
    turn_HSI48_on();
    pc.breakpoint(__LINE__); // breakpoint with actual line number parameter
    turn_PLL_HSI_on();
    pc.breakpoint(__LINE__); // breakpoint with actual line number parameter
    while(1){}     
}


void turn_PLL_HSE_on(){
    if ((RCC->CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_PLL){ // Test if PLL is used as System clock
        RCC->CFGR &= (uint32_t) (~RCC_CFGR_SW); // Select HSI as system clock
        while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI){} //Wait for HSI switched
        RCC->CR &= (uint32_t)(~RCC_CR_PLLON); // Disable the PLL
        while((RCC->CR & RCC_CR_PLLRDY) != 0){} // Wait until PLLRDY is cleared
    }else if( (RCC->CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_HSI48 ){ // Test if HSI48 is used as System clock
        RCC->CFGR &= (uint32_t) (~RCC_CFGR_SW); // Select HSI as system clock
        while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI){} // Wait for HSI switched
        RCC->CR2 &= (uint32_t)(~RCC_CR2_HSI48ON); // Disable the HSI48
        while((RCC->CR2 & RCC_CR2_HSI48RDY) != 0){} //Wait until HSI48RDY is cleared        
    }          
    RCC->CR|=RCC_CR_HSEON; // Enable the HSE clock
    while(!(RCC->CR&RCC_CR_HSERDY)){} //Wait until HSERDY is cleared        
    RCC->CFGR = (RCC->CFGR & (~RCC_CFGR_PLLSRC)) | RCC_CFGR_PLLSRC_HSE_PREDIV; // Set HSE/PREDIV as PLL source clock 
    RCC->CR |= RCC_CR_PLLON; // Enable the PLL
    while((RCC->CR & RCC_CR_PLLRDY) == 0){} // Wait until PLLRDY is set
    RCC->CFGR |= (uint32_t) (RCC_CFGR_SW_PLL); // Select PLL as system clock
    while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL){} // Wait until the PLL is switched on
}

void turn_PLL_HSI_on(){
    if ((RCC->CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_PLL){ // Test if PLL is used as System clock
        RCC->CFGR &= (uint32_t) (~RCC_CFGR_SW); // Select HSI as system clock
        while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI){} // Wait for HSI switched
        RCC->CR &= (uint32_t)(~RCC_CR_PLLON); // Disable the PLL
        while((RCC->CR & RCC_CR_PLLRDY) != 0){} // Wait until PLLRDY is cleared
    }else if( (RCC->CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_HSI48 ){ // Test if HSI48 is used as System clock
        RCC->CFGR &= (uint32_t) (~RCC_CFGR_SW); // Select HSI as system clock
        while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI){} // Wait for HSI switched
        RCC->CR2 &= (uint32_t)(~RCC_CR2_HSI48ON); // Disable the HSI48
        while((RCC->CR2 & RCC_CR2_HSI48RDY) != 0){} //Wait until HSI48RDY is cleared        
    }         
    RCC->CFGR = (RCC->CFGR & (~RCC_CFGR_PLLSRC)) | RCC_CFGR_PLLSRC_HSI_PREDIV; // Set HSI/PREDIV as PLL source clock
    RCC->CR |= RCC_CR_PLLON; //Enable the PLL
    while((RCC->CR & RCC_CR_PLLRDY) == 0){} //Wait until PLLRDY is set
    RCC->CFGR |= (uint32_t) (RCC_CFGR_SW_PLL); //Select PLL as system clock
    while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL){} //Wait until the PLL is switched on
}

void turn_HSI48_on(){
    if ((RCC->CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_PLL){ // Test if PLL is used as System clock
        RCC->CFGR &= (uint32_t) (~RCC_CFGR_SW); // Select HSI as system clock
        while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI){} // Wait for HSI switched
        RCC->CR &= (uint32_t)(~RCC_CR_PLLON); // Disable the PLL
        while((RCC->CR & RCC_CR_PLLRDY) != 0){} // Wait until PLLRDY is cleared
    }else if( (RCC->CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_HSI48 ){ //Test if HSI48 is used as System clock
        return;
    }    
    RCC->CR2|=RCC_CR2_HSI48ON; // Enable the HSI48 clock
    while(!(RCC->CR2&RCC_CR2_HSI48RDY)){} // Wait until HSI48RDY is cleared
    RCC->CFGR |= (uint32_t) (RCC_CFGR_SW_HSI48); // Select HSI48 as system clock
    while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI48){} // Wait until the HSI48 is switched on
}