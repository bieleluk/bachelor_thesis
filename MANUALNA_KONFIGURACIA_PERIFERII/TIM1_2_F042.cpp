#include "mbed.h"
#define max(a, b) (((a) > (b)) ? (a) : (b)) // Macro that returns max value of two input parameters
#define min(a, b) (((a) < (b)) ? (a) : (b)) // Macro that returns max value of two input parameters


PwmOut sensor_simulation(PA_4); // PWM generator on PA4
Serial pc(PA_2, PA_3, 115200); // TX pin, RX pin, baudrate
//Pulse counter on PA9 using TIM1_CH2

volatile float pwm_period = 0.3;

// ISR function to handle serial interrupt
void read_char(){
    char sign = pc.getc(); // get character
    if (sign == 'w' || sign == 'W'){ // Received character is w or W
        pwm_period = min(0.5, pwm_period+0.01); // Increment value of pwm_period by 0.01
    }else if (sign == 's' || sign == 'S'){ // Received character is s or S
        pwm_period = max(0.01, pwm_period-0.01); // Decrement value of pwm_period by 0.01
    }
}

int main() {
    
    RCC->APB2ENR |= RCC_APB2ENR_TIM1EN; // Enable the peripheral clock of Timer 1
    RCC->AHBENR |= RCC_AHBENR_GPIOAEN; // Enable the peripheral clock of GPIOA
    GPIOA->MODER = (GPIOA->MODER & ~(GPIO_MODER_MODER9)) | (GPIO_MODER_MODER9_1); // Select alternate function mode on GPIOA pin 9
    GPIOA->AFR[1] |= 0x2 << GPIO_AFRH_AFSEL9_Pos; //  Select AF2 on PA9 in AFRH for TIM1_CH2
    TIM1->CCMR1 |=  TIM_CCMR1_CC2S_0; // Configure channel 2 to detect rising edges on the TI2
    TIM1->CCMR1 |= TIM_CCMR1_IC2F_0 | TIM_CCMR1_IC2F_1 | TIM_CCMR1_CC2S_0; // Configure the input filter duration
    TIM1->CCER &= (uint16_t)(~TIM_CCER_CC2P); // Select rising edge polarity by writing CC2P=0 in
    TIM1->SMCR |= TIM_SMCR_SMS | TIM_SMCR_TS_2 | TIM_SMCR_TS_1; /* (3) */
    TIM1->CR1 |= TIM_CR1_CEN; // Enable the counter by writing CEN=1
    
    
    sensor_simulation.period(pwm_period); // Set period of PWM on PA4
    sensor_simulation = 0.5; // Set duty cycle of PWM on PA4 to 50%
    pc.attach(&read_char); // Attach read_char function to call whenever a serial interrupt is generated

    while(1){
        pc.printf("\ec"); // Clear the whole screen
        wait_ms(10); // Wait until the screen is cleared
        pc.printf("SIMULATION OF  ENCODER\n\r");
        pc.printf("TIMER VALUE: %4d\n\r",TIM1->CNT); // Print counter value of TIM1
        pc.printf("PWM PERIOD: %4.2f\n\r",pwm_period); // read value of variable pwm_period
        pc.printf("TO INCREASE PWM PERIOD PRESS W, TO DECREASE PWM PERIOD PRESS S");
        sensor_simulation.period(pwm_period); // set PWM period of PA4
        wait(0.3); // wait 300 ms
    }
    
}