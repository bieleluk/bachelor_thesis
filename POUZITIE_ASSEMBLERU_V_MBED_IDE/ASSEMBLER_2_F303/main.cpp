#include "mbed.h"

DigitalOut myled(PA_5);

extern "C" void change_state(uint32_t pin_number);

int main() {
    while(1) {
        change_state(5); // zmena log. hodnoty vystupu PA5
        wait(0.2); // cakanie 200 ms
    }
}
