GPIO_ODR_Offset EQU 0x14 ; adresovy offset pre GPIO ODR register
GPIOA EQU 0x48000000 ; pociatocna adresa GPIOA konfiguracnych registrov

    AREA asm_func, CODE, READONLY
    EXPORT change_state ; export funkcie change_state
change_state ;zaciatok funkcie
    
    PUSH {LR}
    
    ; v R0 je cislo pinu x
    ; nacitanie 0x1 do R1
    LDR R1, =0x1
    ; posunutie hodnoty R1 o hodnotu R0 dolava a ulozenie do R1
    ; (hodnota x bitu bude 1, ostatne budu 0)
    LSL R1, R0
    ; nacitanie pociatocnej adresy konfiguracnych registrov GPIOA do R2
    LDR R2, =GPIOA
    ; nacitanie GPIOA ODR registra do R3
    LDR R3, [R2, #GPIO_ODR_Offset]
    ; logicky sucin(s aktualizovanim flag bitov) R3 a R1 ulozeny do R4
    ; ak bola na vystupe log.0 hodnota R4 bude 0
    ANDS R4,R3,R1
    
    ;konsrukcia ITEE na x-ty bit zapise hodnotu podla flag bitu
    ITEE EQ
    ;ak v R0 je 0, logicky sucet R3 a R1 ulozeny do R3 (x-ty bit do 1)
    ORREQ R3, R1
    ;ak v R0 nie je 0, pouzitie vylucujuceho alebo na R1 s 0xFFFFFFFF
    ; a ulozenie do R1 , (x-ty bit bude 0, ostatne 1)
    EORNE R1, #0xFFFFFFFF
    ;ak v R0 nie je 0, logicky sucet R3 a R1 ulozeny v R3 (x-ty bit do 0)
    ANDNE R3, R1
    
    ; ulozenie zmenenej hodnoty do registra na adrese 0x48000014
    STR R3,[R2,#GPIO_ODR_Offset]; invertovanie hodnoty na x-tom pine
    
    POP {PC}
    
    ALIGN
    END