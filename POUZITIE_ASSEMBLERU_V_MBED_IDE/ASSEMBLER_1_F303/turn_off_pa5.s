GPIO_ODR_Offset EQU 0x14 ;adresovy offset pre GPIO ODR register
LED EQU 5 ;cislo pouziteho pinu
GPIOA EQU 0x48000000 ;pociatocna adresa GPIOA konfiguracnych registrov

    AREA asm_func, CODE, READONLY ;zaciatok funkcneho bloku v assembleri
    EXPORT turn_off_pa5 ;export funkcie turn_off_pa5
turn_off_pa5 ;zaciatok funkcie turn_off_pa5

    PUSH {LR} ; vlozenie LR do zasobnika
    
    ; ulozenie cisla 0x1 pousunuteho o 5 bitov dolava(0x20) do R0
    MOV R0, #(0x01 :SHL: LED)
    ; pouzitie vylucujuceho alebo na R0 s konstantou 0xFFFFFFFF
    ; a ulozenie do R0 , 0x20 XOR 0xFFFFFFFF -> 0xFFFFFFDF
    EOR R0, #0xFFFFFFFF
    ; nacitanie pociatocnej adresy konfiguracnych registrov GPIOA do R1
    LDR R1, =GPIOA
    ; nacitanie hodnoty konfiguracneho registra ODR do R2
    LDR R2, [R1,#GPIO_ODR_Offset]
    ; logicky sucet R2 a R0 ulozeny do R2 (nastavenie 5. bitu do 0)
    AND R2, R0
    ; ulozenie zmenenej hodnoty do registra na adresu 0x48000014
    STR R2,[R1,#GPIO_ODR_Offset] ; LED zhasne
    
    POP {PC} ;odstranenie PC zo zasobnika
    
    ALIGN ;koniec funkceho bloku v assembleri
    END
