;miesto pre definovanie konstant
GPIO_ODR_Offset EQU 0x14; adresovy offset GPIO ODR register
LED EQU 5; cislo pouziteho pinu
GPIOA EQU 0x48000000; pociatocna adresa GPIOA konfiguracnych registrov

    ;zaciatok funkcneho bloku v assembleri
    AREA asm_func, CODE, READONLY
    ;export funkcie turn_on_pa5, funkcia bude viditelna pre compiler
    EXPORT turn_on_pa5
;zaciatok funkcie
turn_on_pa5

    ; vlozenie LR do zasobnika pre uspesny navrat po vykonani funkcie
    PUSH {LR}
    
    ; ulozenie cisla 0x1 pousunuteho o 5 bitov dolava(0x20) do R0
    MOV R0, #(0x01 :SHL: LED)
    ; nacitanie pociatocnej adresy konfiguracnych registrov GPIOA do R1
    LDR R1, =GPIOA
    ; nacitanie hodnoty konfiguracneho registra ODR z adresy GPIOA
    ; posunutej o GPIO_ODR_Offset (0x48000014) do R2
    LDR R2, [R1,#GPIO_ODR_Offset]
    ; logicky sucet R2 a R0 ulozeny do R2 (nastavenie 5. bitu do 1)
    ORR R2, R0
    ; ulozenie hodnoty R2 do ODR na adresu 0x48000014
    STR R2,[R1,#GPIO_ODR_Offset] ;rozsvieti sa LED
    
    ;odstranenie PC zo zasobnika pre navrat z funkcie
    POP {PC}
    
    ;koniec funkceho bloku v assembleri
    ALIGN
    END