#include "mbed.h"

//konfiguracia digitalneho vystupu na PA5 s nazvom myled
DigitalOut myled(PA_5);
//deklaracia funkcie turn_on_pa5 pisanu v assembleri (v turn_on_pa5.s)
extern "C" void turn_on_pa5();
//deklaracia funkcie turn_off_pa5 pisanu v assembleri (v turn_off_pa5.s)
extern "C" void turn_off_pa5();

int main() {
    while(1) {
        turn_on_pa5(); // volanie funkcie turn_on_pa5
        wait(0.5); // cakanie 0.5s
        turn_off_pa5(); // volanie funkcie turn_off_pa5
        wait(0.5); // cakanie 0.5s
    }
}
