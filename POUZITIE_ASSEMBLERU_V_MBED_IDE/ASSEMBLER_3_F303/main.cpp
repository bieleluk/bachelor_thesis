#include "mbed.h"

DigitalOut myled(PA_5);

// deklaracia externej funkcie flash s 2 vstupnymi argumentmi
extern "C" void flash(uint32_t pin, uint32_t period_ms);

int main() {
    while(1) {
        flash(5, 1000); // bliknutie na PA5 s periodou 1000ms
    }
}
