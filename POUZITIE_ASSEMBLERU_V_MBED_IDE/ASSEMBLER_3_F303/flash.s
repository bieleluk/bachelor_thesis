GPIO_BSRR_Offset EQU 0x18; bit set reset register offset
GPIO_BRR_Offset EQU 0x28; bit reset register offset
GPIOA EQU 0x48000000

    AREA asm_func, CODE, READONLY
    EXPORT flash
flash

    PUSH {LR}
    
    ; v R0 je cislo pinu x
    ; v R1 je perioda blikania v ms
    
    ; posunutie hodnoty R1 o 1 bit doprava, celociselne delenie 2
    LSR R1,#1 ; v R1 je polperioda
    ; nacitanie pociatocnej adresy konfiguracnych registrov GPIOA do R2
    LDR R2, =GPIOA
    ; nacitanie 0x1 do R3
    LDR R3, = 0x01
    ; posunutie hodnoty R3 o hodnotu R0 dolava a ulozenie do R3
    ; (hodnota x bitu bude 1, ostatne budu 0)
    LSL R3, R3, R0
    ; ulozenie hodnoty R3 na adresu 0x48000018
    ; nastavenie x-teho bitu BSRR do 1, vystup x-teho pinu do log.1
    STR R3,[R2,#GPIO_BSRR_Offset] ; rozsvieti LED
    ; vykonanie funkcie WAIT_MS so vstupnym argumentom R1
    BL WAIT_MS
    ; ulozenie hodnoty R3 na adresu 0x48000028, nastavenie
    ; nastavenie x-teho bitu BSRR do 1, vystup x-teho pinu do log.0
    STR R3,[R2,#GPIO_BRR_Offset] ;zhasne LED
    ; vykonanie funkcie WAIT_MS so vstupnym argumentom R1
    BL WAIT_MS
    
    POP {PC}
    

; zaciatok procedury WAIT_MS
WAIT_MS PROC
    ; v procedure sa pouzivaju R0 a R1 -> aktualne hodnoty R0 a R1 sa
    ; ulozia do zasobnika, aby sa po skonceni procedury mohli opat pouzit
    ; LR sa ulozi pre navrat do funcie flash po vykonani WAIT_MS
    PUSH {R0,R1,LR}
    ; v R1 je cas v ms
    ; nacitanie 72000 do R0, frekvencia hodin je 72MHz,
    ; pre cakanie 1ms musi cyklus trvat 72000 hodinovych cyklov
    LDR R0, =72000
    ; nasobenie hodnoty registra R1 s R0 a ulozenie do R1
    ; pre cakanie y ms musi cyklus trvat y*72000 hodinovych cyklov
    MUL R1,R0
    
; zaciatok cyklu s nazvom WAITLOOP
WAITLOOP
    ; odcitanie 6 od R1 s aktualizovanim flag bitov
    SUBS R1,#6 ; odcitanie poctu hodinovych cyklov v jednom cykle
    ;prazdna instrukcia
    NOP
    ; podmieneny skok na WAITLOOP, ak v R0 nie je 0
    BNE WAITLOOP
    
    ; priradenie R0 a R1 hodnot v case volania WAIT_MS, navrat do flash
    POP {R0,R1,PC}
    ; koniec procedury
    ENDP
    
    ; koniec funkcneho bloku v assembleri
    ALIGN
    END
